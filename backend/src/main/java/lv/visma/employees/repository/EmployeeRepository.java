package lv.visma.employees.repository;

import lv.visma.employees.dto.EmployeeSimpleDTO;
import lv.visma.employees.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query(nativeQuery = true, value = "SELECT id, firstname, lastname FROM employees")
    Collection<EmployeeSimpleDTO> findAllSimpleDto();

    @Query(nativeQuery = true, value = "SELECT id, firstname, lastname FROM employees LIMIT :limit OFFSET :offset")
    Collection<EmployeeSimpleDTO> findAllSimpleDto(int limit, int offset);

    @Query(nativeQuery = true, value = "SELECT count(*) FROM employees")
    int getEmployeesTotalCount();
}
