package lv.visma.employees.controller;

import lv.visma.employees.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/totalcounts")
public class TotalCountController {

    @Autowired
    private EmployeeRepository employeeRepository;

    public TotalCountController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("/employees")
    int getEmployeesTotalCount(){
        return employeeRepository.getEmployeesTotalCount();
    }
}
