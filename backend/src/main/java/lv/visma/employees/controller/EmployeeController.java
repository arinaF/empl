package lv.visma.employees.controller;

import lv.visma.employees.dto.EmployeeSimpleDTO;
import lv.visma.employees.model.Employee;
import lv.visma.employees.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("/employees")
    Collection<EmployeeSimpleDTO> getEmployees(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        if (page != null && size != null)
            return employeeRepository.findAllSimpleDto(size, (page - 1) * size);
        else
            return employeeRepository.findAllSimpleDto();
    }

    @GetMapping("/employees/{id}")
    Employee getEmployee(@PathVariable Long id) {
        return employeeRepository.getOne(id);
    }

}
